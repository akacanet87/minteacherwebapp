package com.min.controller.main;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.min.domain.Member;
import com.min.lecturecategory.service.LectureCategoryService;
import com.min.member.service.MemberService;

@Controller
@RequestMapping("/")
public class MainController {
	@Autowired
	LectureCategoryService lectureCategoryService;
	@Autowired
	MemberService memberService;
	
	@RequestMapping("index.do")
	public ModelAndView selectAll(){
		List listLecCat = (List)lectureCategoryService.selectAll();
		ModelAndView mav = new ModelAndView();
		mav.addObject("listLecCat", listLecCat);
		mav.setViewName("index");
		return mav;
	}
	@RequestMapping("login.do")
	@ResponseBody
	public void login(@RequestBody Map<String, Object> map){
		Member member = new Member();
		member.setEmail((String)map.get("email"));
		member.setNickname((String)map.get("nickname"));
		
		if(memberService.isMember(member.getEmail()) == 0){
			//insert
			//last_id
			//get Session
			//go to mypage
		}else{
			//select
			//get Session
			//current url
		}
	}
}