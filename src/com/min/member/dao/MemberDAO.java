package com.min.member.dao;

import java.util.List;

import com.min.domain.Member;

public interface MemberDAO {
	public List selectAll();
	public Member select(int member_id);
	public int isMember(String email);
	public int insert(Member member);
	public int delete(int member_id);
	public int update(Member member);
}
