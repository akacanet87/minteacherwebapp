package com.min.member.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.min.domain.Member;

@Repository
public class MemberDAOSpringJdbc implements MemberDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List selectAll() {
		String sql = "select * from member order by member_id desc";
		List list = jdbcTemplate.query(sql, new RowMapper<Member>(){
			@Override
			public Member mapRow(ResultSet rs, int row) throws SQLException {
				Member member = new Member();
				member.setMember_id(rs.getInt("member_id"));
				member.setEmail(rs.getString("email"));
				member.setName(rs.getString("name"));
				member.setNickname(rs.getString("nickname"));
				member.setMypoint(rs.getInt("mypoint"));
				member.setRegdate(rs.getString("regdate"));
				return member;
			}
		});
		return list;
	}	
	@Override
	public Member select(int member_id) {
		String sql = "select * from member where member_id=?";
		Member member = jdbcTemplate.queryForObject(sql, new RowMapper<Member>(){
			@Override
			public Member mapRow(ResultSet rs, int row) throws SQLException {
				Member member = new Member();
				member.setMember_id(rs.getInt("member_id"));
				member.setName(rs.getString("name"));
				member.setEmail(rs.getString("email"));
				member.setNickname(rs.getString("nickname"));
				member.setMypoint(rs.getInt("mypoint"));
				member.setRegdate(rs.getString("regdate"));
				return member;
			}
		}, member_id);
		return member;
	}
	@Override
	public int isMember(String email) {
		String sql = "select count(*) from member where email=?";
		int result = jdbcTemplate.queryForObject(sql, Integer.class, email);
		return result;
	}
	@Override
	public int insert(Member member) {
		String sql = "insert into member(email, name)  values(?, ?)";
		int result = jdbcTemplate.update(sql, new Object[]{
				member.getEmail(),
				member.getName()
		});
		return result;
	}

	@Override
	public int delete(int member_id) {
		String sql = "delete from member where member_id=?";
		int result = jdbcTemplate.update(sql, member_id);
		return result;
	}

	@Override
	public int update(Member member) {
		String sql = "update member set nickname=?, mypoint=? where member_id=?";
		int result = jdbcTemplate.update(sql, new Object[]{
				 member.getNickname(),
				 member.getMypoint(),
				 member.getMember_id()
		});
		return result;
	}
	
}
