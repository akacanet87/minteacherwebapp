package com.min.member.service;

import java.util.List;

import com.min.domain.Member;

public interface MemberService {
	public List selectAll();
	public int isMember(String email);
	public Member select(int member_id);
	public int insert(Member member);
	public int delete(int member_id);
	public int update(Member member);
}
