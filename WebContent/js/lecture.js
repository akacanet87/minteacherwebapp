$(document).ready(function() {
	$('#sendQuestion').on('click', function() {
		if (!confirm('등록하시겠습니까?'))
			return;
	
		var myquestion_content = $('#content').val();
		var lecture_id = $('#lecture_id').val();
		var member_id = $('#member_id').val();
		var json = {
			"content" : myquestion_content,
			"lecture_id" : lecture_id,
			"member_id" : member_id
		}
		$.ajax({
			url : '/myquestion/registQuestion.do',
			type : 'post',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			data : JSON.stringify(json),
			success : function(data) {		
				if(data.reply!=null){
					$('#showQList').prepend(
							'<li><div><p>질문 '+data.myquestion_id+'. '+ data.content
							+ '</p></div><div style="display:block;"><p id="reply_'+data.myquestion_id+'">'+data.myquestion_id+'번 질문에 대한 답변 : '+data.reply+'</p></div></li>');
					
				}else{
					
					$('#showQList').prepend(
							'<li><div><p>질문 '+data.myquestion_id+'. '+ data.content
							+ '</p></div>');
					
				}
				$('#content').val('');
			
			}
		});
	});
	$("registHomework").click(function() {
		if (!confirm('등록하시겠습니까?'))
			return;
		
		var formData = new FormData();
		var file = this.files[0];                      //multiple 속성이 있으면 files 에는 다수의 객체가 할당됨
		formData.append("homework", file);
		
		$.ajax({
			async : true,
			method : 'post',
			url : '/member/sendHomework.do',
			processData : false,
			data : formData,
			contentType : false,
			success : function(data) {
				
				alert("일단 요까지 "+data);
			
			}
		});
	});
});

function goList(){
	
	form1.action="/lecture/list.do";
	form1.submit();
	
}
function registHomework() {

	form1.encoding = "multipart/form-data";
	form1.action = "/";

}